package tn.dkSoft.MyTicket.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.*;
import tn.dkSoft.MyTicket.enums.TicketCategory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@Data
public class TicketsDto  {
    private Long id;
   // @Pattern (regexp = "^[0-9]{5}$", message = "Code Ticket must be a 5-digit number")
    //private int codeTicket;

    @NotNull(message = "Ticket Category is required")
    private TicketCategory ticketCat;

    @Positive(message = "Price must be a positive value")
    private float price;

    @Positive(message = "Quantity must be a positive value")
    private long quantity;

    private String imgUrlTicket;

    @NotNull(message = "Event ID is required")
    private Long eventId;

    private static final Map<TicketCategory, AtomicInteger> categoryCodeCounters = new HashMap<> ();

    static {
        // Initialize code counters for each category with their respective starting values
        categoryCodeCounters.put(TicketCategory.CHAISE, new AtomicInteger(1)); // Starting value for CHAISE
        categoryCodeCounters.put(TicketCategory.GRADIN, new AtomicInteger(10000)); // Starting value for GRADIN
        categoryCodeCounters.put(TicketCategory.BALCON, new AtomicInteger(20000)); // Starting value for BALCON
        categoryCodeCounters.put(TicketCategory.VIP, new AtomicInteger(30000)); // Starting value for VIP
    }

    // Getter and setter methods for the attributes

    public int generateUniqueCodeTicket() {
        if (categoryCodeCounters.containsKey(ticketCat)) {
            AtomicInteger codeCounter = categoryCodeCounters.get(ticketCat);
            int generatedCode = codeCounter.getAndIncrement();
            return generatedCode;
        }
        return 0; // Default if category is not recognized
    }
}

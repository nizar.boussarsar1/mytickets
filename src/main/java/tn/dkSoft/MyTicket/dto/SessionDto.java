package tn.dkSoft.MyTicket.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import tn.dkSoft.MyTicket.model.Venue;

@Data
@Setter
@Getter
@RequiredArgsConstructor
public class SessionDto implements Serializable {
    private Long sessionId;
    @Size(min = 8, max = 15, message = "Session Name must be between 8 and 15 characters long")
    @NotNull(message = "Session Name is required")
    private String sessionName;

    @NotNull(message = "Start Date is required")
    private Date startDate;

    @NotNull(message = "Finish Date is required")
    private Date finishDate;

    private String imgUrl;

    @NotNull(message = "Venue is required")
    private VenueDto venue;
}

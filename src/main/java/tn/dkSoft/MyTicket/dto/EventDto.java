package tn.dkSoft.MyTicket.dto;

import java.util.Date;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.*;
import tn.dkSoft.MyTicket.enums.EventCategory;

@Data
@Setter
@Getter
@RequiredArgsConstructor
public class EventDto {
    private Long eventId;
    @NotEmpty(message = "Description is required.")
    @Pattern(regexp = "^[a-zA-Z]*$", message = "First Name must not contain numbers or special characters")
    private String description;
    private Date dateEvent;
    private EventCategory cat;
    private boolean available;
    private SessionDto session;
}

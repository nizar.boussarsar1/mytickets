package tn.dkSoft.MyTicket.dto;

import jakarta.validation.constraints.Pattern;
import lombok.*;

@Data
@Setter
@Getter
@RequiredArgsConstructor
public class VenueDto {
    private Long venueId;
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Venue must not contain numbers or special characters")
    private String venueName;
    @Pattern(regexp = "^\\d{10}$", message = "Capacity  must contain only Numbers")
    private long capacity;
}

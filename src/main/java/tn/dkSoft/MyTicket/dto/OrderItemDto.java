package tn.dkSoft.MyTicket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {

    private Long orderItemId;
    private Long ticketId;
    private Long orderId;
    private Integer quantity;
    private double orderedTicketPrice;
}

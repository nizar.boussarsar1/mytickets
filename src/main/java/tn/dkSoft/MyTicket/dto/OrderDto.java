package tn.dkSoft.MyTicket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Long orderId;
    private String email;
    private List<OrderItemDto> orderItems= new ArrayList<> ();
   //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    //private LocalDate orderDate;
    private Double totalAmount;
    private String orderStatus;
}

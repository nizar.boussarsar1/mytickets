package tn.dkSoft.MyTicket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tn.dkSoft.MyTicket.model.Order;

import java.util.List;

public interface OrderRepository extends JpaRepository <Order,Long> {


    List<Order> findAllByEmail(String emailId);

    @Query("SELECT o FROM Order o WHERE o.email = ?1 AND o.id = ?2")
    Order findOrderByEmailAndOrderId(String email, Long cartId);
}

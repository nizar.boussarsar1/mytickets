package tn.dkSoft.MyTicket.service.Impl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import tn.dkSoft.MyTicket.dto.SessionDto;
import tn.dkSoft.MyTicket.dto.VenueDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Session;
import tn.dkSoft.MyTicket.model.Venue;
import tn.dkSoft.MyTicket.repository.SessionRepository;
import tn.dkSoft.MyTicket.repository.VenueRepository;
import tn.dkSoft.MyTicket.service.SessionService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
@Validated
public class SessionServiceImpl implements SessionService {

    private final SessionRepository sessionRepository;
    private final VenueRepository venueRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public SessionDto saveSession(@Valid SessionDto sessionDto) {
        log.info("Saving new Session: {}", sessionDto);

        Session session = modelMapper.map(sessionDto, Session.class);

        Long venueId = sessionDto.getVenue().getVenueId();
        if (venueId != null) {
            Venue venue = venueRepository.findById(venueId)
                    .orElseThrow(() -> new NotFoundException("Venue not found"));
            session.setVenue(venue);
        }

        Session savedSession = sessionRepository.save(session);
        SessionDto newSession = modelMapper.map(savedSession, SessionDto.class);

        VenueDto venueDto = modelMapper.map(savedSession.getVenue(), VenueDto.class);
        newSession.setVenue(venueDto);

        log.info("Session saved: {}", newSession);
        return newSession;
    }

    @Override
    public List<SessionDto> listSessions() {
        log.info("Listing all sessions");
        List<Session> sessions = sessionRepository.findAll();

        return sessions.stream()
                .map(session -> {
                    SessionDto sessionDto = modelMapper.map(session, SessionDto.class);
                    sessionDto.setVenue(modelMapper.map(session.getVenue(), VenueDto.class));
                    return sessionDto;
                })
                .collect( Collectors.toList());
    }
    @Override
    public List<SessionDto> searchSession(String keyword) {
        log.info("Searching for session with keyword: {}", keyword);
        List<Session> sessions = sessionRepository.searchSession(keyword);

        return sessions.stream()
                .map(session -> {
                    SessionDto sessionDto = modelMapper.map(session, SessionDto.class);
                    sessionDto.setVenue(modelMapper.map(session.getVenue(), VenueDto.class));
                    return sessionDto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public SessionDto getSession(Long id) throws NotFoundException {
        log.info("Fetching session with ID: {}", id);
        Session session = sessionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Session not found"));

        SessionDto sessionDto = modelMapper.map(session, SessionDto.class);

        if (session.getVenue() != null) {
            VenueDto venueDto = modelMapper.map(session.getVenue(), VenueDto.class);
            sessionDto.setVenue(venueDto);
        }

        return sessionDto;
    }
    @Override
    public SessionDto updateSession(@Valid SessionDto sessionDto) {
        log.info("Updating Session: {}", sessionDto);

        Long sessionId = sessionDto.getSessionId();
        Session existingSession = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new NotFoundException("Session not found"));
        modelMapper.map(sessionDto, existingSession);

        if (sessionDto.getVenue() != null) {
            Long venueId = sessionDto.getVenue().getVenueId();
            Venue venue = venueRepository.findById(venueId)
                    .orElseThrow(() -> new NotFoundException("Venue not found"));
            existingSession.setVenue(venue);
        } else {
            existingSession.setVenue(null);
        }

        Session savedSession = sessionRepository.save(existingSession);

        SessionDto updatedSessionDto = modelMapper.map(savedSession, SessionDto.class);

        if (savedSession.getVenue() != null) {
            VenueDto venueDto = modelMapper.map(savedSession.getVenue(), VenueDto.class);
            updatedSessionDto.setVenue(venueDto);
        }

        log.info("Session updated: {}", savedSession);
        return updatedSessionDto;
    }
    @Override
    public void deleteSession(Long id) {
        log.info("Deleting session with ID: {}", id);
        sessionRepository.deleteById(id);
    }



}

package tn.dkSoft.MyTicket.service.Impl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.dto.EventDto;
import tn.dkSoft.MyTicket.dto.SessionDto;
import tn.dkSoft.MyTicket.dto.TicketsDto;
import tn.dkSoft.MyTicket.dto.VenueDto;
import tn.dkSoft.MyTicket.enums.TicketCategory;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Event;
import tn.dkSoft.MyTicket.model.Session;
import tn.dkSoft.MyTicket.model.Tickets;
import tn.dkSoft.MyTicket.model.Venue;
import tn.dkSoft.MyTicket.repository.EventRepository;
import tn.dkSoft.MyTicket.repository.TicketRepository;
import tn.dkSoft.MyTicket.service.TicketService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class TicketServiceImpl implements TicketService {
    private final EventRepository eventRepository;
    private final TicketRepository ticketRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public TicketsDto saveTickets(@Valid TicketsDto ticketsDto) {
        log.info("Saving new Ticket: {}", ticketsDto);

        Tickets tickets = modelMapper.map(ticketsDto, Tickets.class);
        Long eventId = ticketsDto.getEventId();
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new NotFoundException("Event not found"));
        tickets.setEvent(event);

        Tickets savedTickets = ticketRepository.save(tickets);
        TicketsDto newTicket = modelMapper.map(savedTickets, TicketsDto.class);

        EventDto eventDto = modelMapper.map(savedTickets.getEvent(), EventDto.class);

        SessionDto sessionDto = new SessionDto();
        Session session = savedTickets.getEvent().getSession();
        if (session != null) {
            sessionDto = modelMapper.map(session, SessionDto.class);
            Venue venue = session.getVenue();
            if (venue != null) {
                VenueDto venueDto = modelMapper.map(venue, VenueDto.class);
                sessionDto.setVenue(venueDto);
            }
        }

        eventDto.setSession(sessionDto);
        newTicket.setEventId(eventId);

        log.info("Ticket saved: {}", newTicket);
        return newTicket;
    }

    @Override
    public List<TicketsDto> listTickets() {
        log.info("Listing all tickets with details");

        List<Tickets> ticketsList = ticketRepository.findAll();
        List<TicketsDto> ticketsDtoList = ticketsList.stream()
                .map(ticket -> {
                    TicketsDto ticketsDto = modelMapper.map(ticket, TicketsDto.class);
                    EventDto eventDto = modelMapper.map(ticket.getEvent(), EventDto.class);
                    SessionDto sessionDto = modelMapper.map(ticket.getEvent().getSession(), SessionDto.class);

                    Venue venue = ticket.getEvent().getSession().getVenue();
                    if (venue != null) {
                        VenueDto venueDto = modelMapper.map(venue, VenueDto.class);
                        sessionDto.setVenue(venueDto);
                    }

                    eventDto.setSession(sessionDto);
                    ticketsDto.setEventId( eventDto.getEventId () );
                    return ticketsDto;
                })
                .collect( Collectors.toList());

        return ticketsDtoList;
    }

    @Override
    public List<TicketsDto> searchTicketsByEventIdAndCategory(Long eventId, TicketCategory category) {
        log.info("Searching for tickets with eventId: {} and category: {}", eventId, category);
        List<Tickets> tickets = ticketRepository.searchTicketsByEventIdAndCategory(eventId, category);

        List<TicketsDto> ticketsDtoList = tickets.stream()
                .map(ticket -> {
                    TicketsDto ticketsDto = modelMapper.map(ticket, TicketsDto.class);
                    EventDto eventDto = modelMapper.map(ticket.getEvent(), EventDto.class);
                    SessionDto sessionDto = modelMapper.map(ticket.getEvent().getSession(), SessionDto.class);
                    Venue venue = ticket.getEvent().getSession().getVenue();
                    if (venue != null) {
                        VenueDto venueDto = modelMapper.map(venue, VenueDto.class);
                        sessionDto.setVenue(venueDto);
                    }
                    eventDto.setSession(sessionDto);
                    ticketsDto.setEventId(eventId);
                    return ticketsDto;
                })
                .collect(Collectors.toList());

        return ticketsDtoList;
    }

@Override
    public TicketsDto updateTickets(@Valid TicketsDto ticketsDto) throws NotFoundException {
        log.info("Updating Tickets: {}", ticketsDto.getId());

        // Map the DTO to the entity
        Tickets ticket = modelMapper.map(ticketsDto, Tickets.class);

        // Check if the ticket with the given ID exists
        ticketRepository.findById(ticketsDto.getId())
                .orElseThrow(() -> new NotFoundException("Ticket not found"));

        // Save the updated entity
        Tickets updatedTicket = ticketRepository.save(ticket);

        // Map the updated entity back to a DTO
        TicketsDto updatedTicketsDto = modelMapper.map(updatedTicket, TicketsDto.class);

        log.info("Ticket updated: {}", updatedTicketsDto);

        return updatedTicketsDto;
    }
    @Override
    public void deleteTickets(Long id) {
        log.info ( "Deleting ticket with ID: {}", id );
        ticketRepository.deleteById ( id );
    }

}


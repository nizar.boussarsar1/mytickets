package tn.dkSoft.MyTicket.service.Impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tn.dkSoft.MyTicket.dto.OrderDto;
import tn.dkSoft.MyTicket.dto.OrderResponse;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.mappers.MapperImpl;
import tn.dkSoft.MyTicket.model.*;
import tn.dkSoft.MyTicket.repository.CartRepository;
import tn.dkSoft.MyTicket.repository.OrderItemRepository;
import tn.dkSoft.MyTicket.repository.OrderRepository;
import tn.dkSoft.MyTicket.repository.UserRepository;
import tn.dkSoft.MyTicket.service.CartService;
import tn.dkSoft.MyTicket.service.OrderService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ModelMapper modelMapper;

private final CartRepository cartRepository;
    @Autowired
private final OrderRepository orderRepository;
    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final OrderItemRepository orderItemRepository;
    @Autowired
    private final CartService cartService;
    @Override
    public OrderDto placeOrder(String emailId, Long cartId) throws Exception {
        Cart cart = cartRepository.findCartByEmailAndCartId ( emailId,cartId );

        if (cart == null) {
            throw new NotFoundException ("Cart cartId"+ cartId);
        }

        List<CartItem> cartItems = cart.getCartItems();

        if (cartItems.size() == 0) {
            throw new Exception ("Cart is empty please check your cartShopping");
        }
        Order order = new Order ();
order.setEmail ( emailId );
order.setOrderDate ( LocalDate.now () );
order.setTotalAmount(cart.getTotalPrice());
order.setOrderStatus("Order Accepted !");
        Order savedOrder = orderRepository.save(order);


            List<OrderItem> orderItems = new ArrayList<> ();

        for (CartItem cartItem : cartItems) {
            OrderItem orderItem = new OrderItem();

            orderItem.setTickets (cartItem.getTickets ());
            orderItem.setQuantity(cartItem.getQuantity());
            orderItem.setOrderedTicketPrice (cartItem.getTicketPrice ());
            orderItem.setOrder(savedOrder);

            orderItems.add(orderItem);
        }
        orderItems = orderItemRepository.saveAll(orderItems);

        cart.getCartItems().forEach(item -> {
            int quantity = item.getQuantity();

            Tickets tickets = item.getTickets ();

            cartService.deleteTicketFromCart (cartId, item.getTickets ().getId ());

            tickets.setQuantity(tickets.getQuantity() - quantity);
        });

        OrderDto orderDto = modelMapper.map(order,OrderDto.class );



        return orderDto;
    }

    @Override
    public OrderResponse getAllOrderss(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder) {

        Sort sortByAndOrder = sortOrder.equalsIgnoreCase("asc") ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageDetails = PageRequest.of(pageNumber, pageSize, sortByAndOrder);

        Page<Order> pageProducts = orderRepository.findAll(pageDetails);

        List<Order> products = pageProducts.getContent();

        List<OrderDto> orderDtos = products.stream().map( order -> MapperImpl.fromOrder ( order ))
                .collect( Collectors.toList());

        OrderResponse orderResponse = new OrderResponse ();

        orderResponse.setContent(orderDtos);
        orderResponse.setPageNumber(pageProducts.getNumber());
        orderResponse.setPageSize(pageProducts.getSize());
        orderResponse.setTotalElements(pageProducts.getTotalElements());
        orderResponse.setTotalPages(pageProducts.getTotalPages());
        orderResponse.setLastPage(pageProducts.isLast());

        return orderResponse;
    }
    @Override
    public List<OrderDto> getOrdersByUser(String emailId) throws Exception {
        List<Order> orders = orderRepository.findAllByEmail(emailId);

        List<OrderDto> orderDTOs = orders.stream()
                .map( MapperImpl::fromOrder )
                .collect(Collectors.toList());

        if (orderDTOs.size() == 0) {
            throw new Exception ("No orders placed yet by the user with email: " + emailId);
        }

        return orderDTOs;
    }

    @Override
    public OrderDto getOrder(String emailId, Long orderId) {

        Order order = orderRepository.findOrderByEmailAndOrderId(emailId, orderId);

        if (order == null) {
            throw new NotFoundException ("Order , OrderId" + orderId);
        }

        return MapperImpl.fromOrder ( order );
    }

    @Override
    public OrderDto updateOrder(String emailId, Long orderId, String orderStatus) {

        Order order = orderRepository.findOrderByEmailAndOrderId(emailId, orderId);

        if (order == null) {
            throw new NotFoundException ("Order orderId"+ orderId);
        }

        order.setOrderStatus(orderStatus);

        return modelMapper.map(order, OrderDto.class);
    }

}

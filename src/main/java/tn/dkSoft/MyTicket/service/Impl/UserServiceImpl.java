package tn.dkSoft.MyTicket.service.Impl;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.exceptions.UserAlreadyExistsException;
import tn.dkSoft.MyTicket.model.Cart;
import tn.dkSoft.MyTicket.model.CartItem;
import tn.dkSoft.MyTicket.model.User;
import tn.dkSoft.MyTicket.repository.CartRepository;
import tn.dkSoft.MyTicket.repository.UserRepository;
import tn.dkSoft.MyTicket.service.CartService;
import tn.dkSoft.MyTicket.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
@Getter
@Setter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder
@Slf4j
@Transactional
class UserServiceImpl implements UserService {
    @Autowired
    private final CartRepository cartRepository;
    @Autowired
    private final UserRepository userRepository;
    private final CartService cartService;
    private final ModelMapper modelMapper;

    @Override
    public User registerUser(User user) throws UserAlreadyExistsException {
        try {
            Optional<User> existUser = userRepository.findByEmail(user.getEmail());
            if (existUser.isPresent()) {
                throw new UserAlreadyExistsException("User already exists with emailId: " + user.getEmail());
            }

            Cart cart = new Cart();
            user.setCart(cart);
            cart.setUser(user);

            user = userRepository.save(user);
            cartRepository.save(cart);

            log.info("User registered successfully: {}", user);

            return user;
        } catch (DataIntegrityViolationException e) {
            log.error("Error registering user: " + e.getMessage(), e);
            throw new UserAlreadyExistsException ("User already exists with emailId: " + user.getEmail());
        }
    }


    @Override
    public List<User> getAllUsers() {
        try {
            assert userRepository != null;
            List<User> users = userRepository.findAll();
            log.info("Fetched {} users from the database", users.size());
            return users;
        } catch (Exception e) {
            log.error("Error fetching users: " + e.getMessage(), e);
            throw new RuntimeException("Error fetching users", e);
        }
    }

    @Override
    public User getUserById(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            User foundUser = user.get();
            if (foundUser.getCart() != null) {
                List<CartItem> cartItems = foundUser.getCart().getCartItems();
                foundUser.getCart().setCartItems (cartItems);
            }
            return foundUser;
        } else {
            throw new NotFoundException("User not found with ID: " + userId);
        }
    }



    @Override
    public User updateUser(User user) throws NotFoundException {
        try {
            // Check if the user exists
            Optional<User> existingUser = userRepository.findById(user.getUserId());
            if (existingUser.isEmpty()) {
                throw new NotFoundException("User not found with ID: " + user.getUserId());
            }

            User updatedUser = userRepository.save(user);

            log.info("User updated successfully: {}", updatedUser);

            return updatedUser;
        } catch (DataIntegrityViolationException e) {
            log.error("Error updating user: " + e.getMessage(), e);
            throw new NotFoundException("User not found with ID: " + user.getUserId());
        }
    }

    @Override
    public void deleteUser(Long userId) {
        log.info("Deleting User with ID: {}", userId);
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found"));
       // List<CartItem> cartItems = user.getCart().getCartItems();
        //Long cartId = user.getCart().getCartId();

       /* cartItems.forEach(item -> {
            Long ticketId = item.getTickets().getId();
            cartService.deleteTicketFromCart(cartId, ticketId);
        });*/

        userRepository.deleteById(userId);
    }
@Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail (email);}
}

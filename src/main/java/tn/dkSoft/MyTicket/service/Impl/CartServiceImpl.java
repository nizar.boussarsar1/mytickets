package tn.dkSoft.MyTicket.service.Impl;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.dto.CartDto;
import tn.dkSoft.MyTicket.dto.CartItemDto;
import tn.dkSoft.MyTicket.dto.TicketsDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Cart;
import tn.dkSoft.MyTicket.model.CartItem;
import tn.dkSoft.MyTicket.model.Tickets;
import tn.dkSoft.MyTicket.repository.CartItemRepository;
import tn.dkSoft.MyTicket.repository.CartRepository;
import tn.dkSoft.MyTicket.repository.TicketRepository;
import tn.dkSoft.MyTicket.service.CartService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Slf4j
@Transactional
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    public CartRepository cartRepository;
    @Autowired
    public CartItemRepository cartItemRepository;
    @Autowired
    public TicketRepository ticketRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Override

    public CartDto addTicketToCart(Long cartId, Long ticketId, Integer quantity) throws Exception {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException("Cart not found"));

        Tickets ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException("Ticket not found"));

        if (ticket.getQuantity() == 0) {
            throw new Exception(ticket.getTicketCat() + " is not available");
        }

        if (ticket.getQuantity() < quantity) {
            throw new Exception("Please, make an order of the " + ticket.getTicketCat()
                    + " less than or equal to the quantity " + ticket.getQuantity() + ".");
        }

        CartItem newCartItem = new CartItem();
        newCartItem.setTickets (ticket);
        newCartItem.setCart(cart);
        newCartItem.setQuantity(quantity);
        newCartItem.setTicketPrice(ticket.getPrice());

        cartItemRepository.save(newCartItem);

        ticket.setQuantity(ticket.getQuantity() - quantity);
        ticketRepository.save(ticket);

        // Update the total price in the cart
        cart.setTotalPrice(cart.getTotalPrice() + (ticket.getPrice() * quantity));
        cartRepository.save(cart);

        // Create the CartDto with updated cart information
        CartDto cartDto = modelMapper.map(cart, CartDto.class);

        // Retrieve cart items, including the newly added item
        List<CartItemDto> cartItemDtos = cart.getCartItems().stream()
                .map(cartItem -> {
                    CartItemDto cartItemDto = modelMapper.map(cartItem, CartItemDto.class);
                    cartItemDto.setTicketsDto (modelMapper.map(cartItem.getTickets (), TicketsDto.class));
                    return cartItemDto;
                })
                .collect( Collectors.toList());

        // Add the newly added item to the list of cart items
        cartItemDtos.add(modelMapper.map(newCartItem, CartItemDto.class));

        cartDto.setCartItemDtos(cartItemDtos);

        return cartDto;
    }

    @Override
    public List<CartDto> getAllCarts() throws Exception {
        List<Cart> carts = cartRepository.findAll();

        if (carts.isEmpty()) {
            throw new Exception("No cart exists");
        }

        return carts.stream()
                .map(cart -> {
                    CartDto cartDTO = modelMapper.map(cart, CartDto.class);
                    List<CartItemDto> cartItemDtos = cart.getCartItems().stream()
                            .map(cartItem -> modelMapper.map(cartItem, CartItemDto.class))
                            .collect(Collectors.toList());
                    cartDTO.setCartItemDtos(cartItemDtos);
                    return cartDTO;
                })
                .collect(Collectors.toList());
    }

    public CartDto getCartById(Long cartId) throws Exception {
        Optional<Cart> optionalCart = cartRepository.findById(cartId);

        if (!optionalCart.isPresent()) {
            throw new Exception("Cart not found with ID " + cartId);
        }

        Cart cart = optionalCart.get();
        CartDto cartDTO = modelMapper.map(cart, CartDto.class);

        List<CartItemDto> cartItemDtos = cart.getCartItems().stream()
                .map(cartItem -> {
                    CartItemDto cartItemDto = modelMapper.map(cartItem, CartItemDto.class);

                    // Fetch ticket details for the cart item here
                    Tickets ticket = cartItem.getTickets();
                    // Map ticket details to TicketDto if needed
                    TicketsDto ticketDto = modelMapper.map(ticket, TicketsDto.class);
                    cartItemDto.setTicketsDto(ticketDto);

                    return cartItemDto;
                })
                .collect(Collectors.toList());

        cartDTO.setCartItemDtos(cartItemDtos);

        return cartDTO;
    }

    @Override
    public void updateTicketInCarts(Long cartId, Long ticketId) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException("CartId" + cartId));

        Tickets ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException ("Ticket ticketId" +ticketId));

        CartItem cartItem = cartItemRepository.findCartItemByTicketIdAndCartId (cartId, ticketId);

        if (cartItem == null) {
            throw new NotFoundException ("Ticket " + ticket.getTicketCat() + " not available in the cart!!!");
        }

        double previousCartItemPrice = cartItem.getTicketPrice() * cartItem.getQuantity();
        double newCartItemPrice = ticket.getPrice() * cartItem.getQuantity();
        double priceDifference = newCartItemPrice - previousCartItemPrice;

        cartItem.setTicketPrice(ticket.getPrice());

        cart.setTotalPrice(cart.getTotalPrice() + priceDifference);

        cartItem = cartItemRepository.save(cartItem);

        cartRepository.save(cart);
    }

    @Override
    public CartDto updateTicketQuantityInCart(Long cartId, Long ticketId, Integer quantity) throws Exception {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException ("Cart cartId"+cartId));

        Tickets tickets = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException ("TicketId "+ticketId));

        if (tickets.getQuantity() == 0) {
            throw new Exception (tickets.getEvent ().getDescription () + " is not available");
        }

        if (tickets.getQuantity() < quantity) {
            throw new Exception ("Please, make an order of the " + tickets.getEvent ().getDescription ()
                    + " less than or equal to the quantity " + tickets.getQuantity() + ".");
        }

        CartItem cartItem = cartItemRepository.findCartItemByTicketIdAndCartId (cartId, ticketId);

        if (cartItem == null) {
            throw new Exception ("Product " + tickets.getEvent ().getDescription ()+ " not available in the cart!!!");
        }

        double cartPrice = cart.getTotalPrice() - (cartItem.getTicketPrice () * cartItem.getQuantity());

        tickets.setQuantity(tickets.getQuantity() + cartItem.getQuantity() - quantity);

        cartItem.setQuantity(quantity);


        cart.setTotalPrice(cartPrice + (cartItem.getTicketPrice () * quantity));

        cartItem = cartItemRepository.save(cartItem);

        CartDto cartDto = modelMapper.map(cart, CartDto.class);

        List<CartItemDto> cartItemDtos = cart.getCartItems().stream()
                .map(cartItemDto -> modelMapper.map(cartItemDto, CartItemDto.class))
                .collect(Collectors.toList());

        cartDto.setCartItemDtos(cartItemDtos);

        return cartDto;

    }

    @Override
    public String deleteTicketFromCart(Long cartId, Long ticketId) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException ("Cart cartId" +cartId));

        CartItem cartItem = cartItemRepository.findCartItemByTicketIdAndCartId(cartId, ticketId);

        if (cartItem == null) {
            throw new NotFoundException ("Ticket ticketId" +ticketId);
        }

        cart.setTotalPrice(cart.getTotalPrice() - (cartItem.getTicketPrice() * cartItem.getQuantity()));

        Tickets ticket = cartItem.getTickets ();
        ticket.setQuantity(ticket.getQuantity() + cartItem.getQuantity());

        cartItemRepository.deleteCartItemByTicketIdAndCartId(cartId, ticketId);

        return "Ticket " + cartItem.getTickets ().getTicketCat() + " removed from the cart !!!";
    }

}

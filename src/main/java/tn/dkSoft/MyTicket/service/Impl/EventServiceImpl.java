package tn.dkSoft.MyTicket.service.Impl;

import java.util.List;
import java.util.stream.Collectors;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import tn.dkSoft.MyTicket.dto.EventDto;
import tn.dkSoft.MyTicket.dto.EventResponse;
import tn.dkSoft.MyTicket.dto.SessionDto;
import tn.dkSoft.MyTicket.dto.VenueDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Event;
import tn.dkSoft.MyTicket.model.Session;
import tn.dkSoft.MyTicket.repository.EventRepository;
import tn.dkSoft.MyTicket.repository.SessionRepository;
import tn.dkSoft.MyTicket.repository.VenueRepository;
import tn.dkSoft.MyTicket.service.EventService;

@Service
@Slf4j
@Transactional
@Validated
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {
    private final VenueRepository venueRepository;
    private final EventRepository eventRepository;
    private final SessionRepository sessionRepository;

    private final ModelMapper modelMapper;


    @Override
    @Transactional
    public EventDto saveEvent(@Valid EventDto eventDto) {
        log.info("Saving new Event: {}", eventDto);

        Event event = modelMapper.map(eventDto, Event.class);

        Long sessionId = eventDto.getSession().getSessionId();
        if (sessionId != null) {
            Session session = sessionRepository.findById(sessionId)
                    .orElseThrow(() -> new NotFoundException("Session not found"));
            event.setSession(session);
        }

        Event savedEvent = eventRepository.save(event);

        EventDto newEvent = modelMapper.map(savedEvent, EventDto.class);

        SessionDto sessionDto = modelMapper.map(savedEvent.getSession(), SessionDto.class);
        VenueDto venueDto = modelMapper.map(savedEvent.getSession().getVenue(), VenueDto.class);
        sessionDto.setVenue(venueDto);
        newEvent.setSession(sessionDto);

        log.info("Event saved: {}", newEvent);
        return newEvent;
    }
    @Override
    public List<EventDto> listEvent() {
        log.info("Listing all events");
        List<Event> events = eventRepository.findAll();
        return events.stream()
                .map(event -> {
                    EventDto eventDto = modelMapper.map(event, EventDto.class);
                    SessionDto sessionDto = modelMapper.map(event.getSession(), SessionDto.class);
                    if (event.getSession() != null && event.getSession().getVenue() != null) {
                        VenueDto venueDto = modelMapper.map(event.getSession().getVenue(), VenueDto.class);
                        sessionDto.setVenue(venueDto);
                    }
                    eventDto.setSession(sessionDto);
                    return eventDto;
                })
                .collect(Collectors.toList());
    }


    @Override
    public EventResponse getAllEvents(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder) {

        Sort sortByAndOrder = sortOrder.equalsIgnoreCase("asc") ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageDetails = PageRequest.of(pageNumber, pageSize, sortByAndOrder);

        Page<Event> pageProducts = eventRepository.findAll(pageDetails);

        List<Event> products = pageProducts.getContent();

        List<EventDto> eventDtos = products.stream().map( event -> modelMapper.map(event, EventDto.class))
                .collect( Collectors.toList());

        EventResponse eventResponse = new EventResponse ();

        eventResponse.setContent(eventDtos);
        eventResponse.setPageNumber(pageProducts.getNumber());
        eventResponse.setPageSize(pageProducts.getSize());
        eventResponse.setTotalElements(pageProducts.getTotalElements());
        eventResponse.setTotalPages(pageProducts.getTotalPages());
        eventResponse.setLastPage(pageProducts.isLast());

        return eventResponse;
    }


    @Override
    public List<EventDto> searchEvent(String keyword) {
        log.info("Searching for events with keyword: {}", keyword);
        List<Event> events = eventRepository.searchEvent(keyword);

        return events.stream()
                .map(event -> {
                    EventDto eventDto = modelMapper.map(event, EventDto.class);
                    SessionDto sessionDto = modelMapper.map(event.getSession(), SessionDto.class);

                    if (event.getSession() != null && event.getSession().getVenue() != null) {
                        VenueDto venueDto = modelMapper.map(event.getSession().getVenue(), VenueDto.class);
                        sessionDto.setVenue(venueDto);
                    }

                    eventDto.setSession(sessionDto);

                    return eventDto;
                })
                .collect(Collectors.toList());
    }
    @Override
    public EventDto getEvent(Long id) throws NotFoundException {
        log.info("Fetching event with ID: {}", id);
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Event not found"));

        EventDto eventDto = modelMapper.map(event, EventDto.class);
        SessionDto sessionDto = modelMapper.map(event.getSession(), SessionDto.class);

        if (event.getSession() != null && event.getSession().getVenue() != null) {
            VenueDto venueDto = modelMapper.map(event.getSession().getVenue(), VenueDto.class);
            sessionDto.setVenue(venueDto);
        }

        eventDto.setSession(sessionDto);

        return eventDto;
    }

    @Override
    public void deleteEvent(Long id) {
        log.info("Deleting event with ID: {}", id);
        eventRepository.deleteById(id);
    }

        }

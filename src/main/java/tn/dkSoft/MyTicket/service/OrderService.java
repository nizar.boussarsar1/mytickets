package tn.dkSoft.MyTicket.service;

import tn.dkSoft.MyTicket.dto.OrderDto;
import tn.dkSoft.MyTicket.dto.OrderResponse;

import java.util.List;

public interface OrderService {


    OrderDto placeOrder(String emailId, Long cartId) throws Exception;

    OrderResponse getAllOrderss(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder);

    List<OrderDto> getOrdersByUser(String emailId) throws Exception;

    OrderDto getOrder(String emailId, Long orderId);

    OrderDto updateOrder(String emailId, Long orderId, String orderStatus);
}

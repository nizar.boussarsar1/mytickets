package tn.dkSoft.MyTicket.service;

import java.util.List;

import jakarta.validation.Valid;
import tn.dkSoft.MyTicket.dto.VenueDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Venue;

public interface VenueService {

    VenueDto saveVenue(@Valid VenueDto venueDto);

    List<VenueDto> listVenue();

    VenueDto getVenue(Long id) throws NotFoundException;

    VenueDto updateVenue(@Valid VenueDto venueDto);

    void deleteVenue(Long id);
}

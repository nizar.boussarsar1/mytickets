package tn.dkSoft.MyTicket.service;

import jakarta.validation.Valid;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.dto.EventDto;
import tn.dkSoft.MyTicket.dto.EventResponse;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Event;

import java.util.List;

public interface EventService {


    @Transactional
    EventDto saveEvent(@Valid EventDto eventDto);

    List<EventDto> listEvent();

    EventResponse getAllEvents(Integer pageNumber, Integer pageSize, String sortBy, String sortOrder);


    List<EventDto> searchEvent(String keyword);

    EventDto getEvent(Long id) throws NotFoundException;

    void deleteEvent(Long id);
}

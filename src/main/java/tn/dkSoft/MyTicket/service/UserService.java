package tn.dkSoft.MyTicket.service;

import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    //User registerUser(CreateUserRequest createUserRequest) throws Exception;

    User registerUser(User user) throws Exception;

    List<User> getAllUsers();

    User getUserById(Long userId);


   // @Transactional
   // User updateUser(Long userId, CreateUserRequest createUserRequest);

    User updateUser(User user) throws NotFoundException;

    void deleteUser(Long userId);

    Optional<User> findUserByEmail(String email);
}

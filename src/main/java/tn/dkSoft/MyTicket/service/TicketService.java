package tn.dkSoft.MyTicket.service;

import java.util.List;

import jakarta.validation.Valid;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.dto.TicketsDto;
import tn.dkSoft.MyTicket.enums.TicketCategory;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Tickets;

public interface TicketService {


    @Transactional
    TicketsDto saveTickets(@Valid TicketsDto ticketsDto);

    List<TicketsDto> listTickets();

    List<TicketsDto> searchTicketsByEventIdAndCategory(Long eventId, TicketCategory category);

    TicketsDto updateTickets(@Valid TicketsDto ticketsDto) throws NotFoundException;

    void deleteTickets(Long id);
}

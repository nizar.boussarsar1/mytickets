package tn.dkSoft.MyTicket.service;

import java.util.List;

import jakarta.validation.Valid;
import org.springframework.transaction.annotation.Transactional;
import tn.dkSoft.MyTicket.dto.SessionDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.Session;

public interface SessionService {
    @Transactional
    SessionDto saveSession(@Valid SessionDto sessionDto);

    List<SessionDto> listSessions();

    List<SessionDto> searchSession(String keyword);

    SessionDto getSession(Long id) throws NotFoundException;

    SessionDto updateSession(@Valid SessionDto sessionDto);

    void deleteSession(Long id);
/*
    Session saveSession(Session session);

    List<Session> listSessions();

    List<Session> searchSession(String keyword);

    Session getSession(Long id) throws NotFoundException;

    void deleteSession(Long id);*/
}

package tn.dkSoft.MyTicket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

import java.io.IOException;

@SpringBootApplication
@EnableMethodSecurity(prePostEnabled = true, securedEnabled = true,jsr250Enabled = true)
public class MyTicketApplication {


    public static void main(String[] args) throws IOException {
        SpringApplication.run(MyTicketApplication.class, args);
    }


}

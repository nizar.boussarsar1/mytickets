package tn.dkSoft.MyTicket.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tn.dkSoft.MyTicket.dto.EventResponse;
import tn.dkSoft.MyTicket.dto.OrderDto;
import tn.dkSoft.MyTicket.dto.OrderResponse;
import tn.dkSoft.MyTicket.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Service
@Validated
@Slf4j
public class OrderController {
    @Autowired
    public OrderService orderService;

   @PostMapping("/users/{emailId}/carts/{cartId}/order")
    public ResponseEntity<OrderDto> orderProducts(@PathVariable String emailId, @PathVariable Long cartId) throws Exception {
        OrderDto order = orderService.placeOrder (emailId, cartId);

        return new ResponseEntity<OrderDto>(order, HttpStatus.CREATED);
    }

    @GetMapping("/public/orders")
    public ResponseEntity<OrderResponse> getAllOrders(
            @RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2", required = false) Integer pageSize,
            @RequestParam(name = "sortBy", defaultValue = "orderId", required = false) String sortBy,
            @RequestParam(name = "sortOrder", defaultValue = "asc", required = false) String sortOrder) {

        OrderResponse orderResponse = orderService.getAllOrderss (pageNumber, pageSize, sortBy, sortOrder);

        return new ResponseEntity<OrderResponse>(orderResponse, HttpStatus.FOUND);
    }


    @GetMapping("public/users/{email}")
    public ResponseEntity<List<OrderDto>> getOrdersByUser(@PathVariable String email) throws Exception {
        List<OrderDto> orders = orderService.getOrdersByUser (email);

        return new ResponseEntity<List<OrderDto>>(orders, HttpStatus.FOUND);
    }

    @GetMapping("public/users/{emailId}/orders/{orderId}")
    public ResponseEntity<OrderDto> getOrderByUser(@PathVariable String emailId, @PathVariable Long orderId) {
        OrderDto order = orderService.getOrder(emailId, orderId);

        return new ResponseEntity<OrderDto>(order, HttpStatus.FOUND);
    }

    @PutMapping("admin/users/{emailId}/orders/{orderId}/orderStatus/{orderStatus}")
    public ResponseEntity<OrderDto> updateOrderByUser(@PathVariable String emailId, @PathVariable Long orderId, @PathVariable String orderStatus) {
        OrderDto order = orderService.updateOrder(emailId, orderId, orderStatus);

        return new ResponseEntity<OrderDto>(order, HttpStatus.OK);
    }

}

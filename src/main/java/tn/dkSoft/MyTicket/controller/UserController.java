package tn.dkSoft.MyTicket.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tn.dkSoft.MyTicket.dto.CreateUserRequest;
import tn.dkSoft.MyTicket.dto.UserDto;
import tn.dkSoft.MyTicket.exceptions.NotFoundException;
import tn.dkSoft.MyTicket.model.User;
import tn.dkSoft.MyTicket.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;
    private final ModelMapper modelMapper;
    private static final Logger logger = LoggerFactory.getLogger(TicketController.class);


    @PostMapping("/public/user/register")
    public ResponseEntity<UserDto> registerUser(@RequestBody CreateUserRequest createUserRequest) {
        User user = modelMapper.map(createUserRequest, User.class);

            try {

                user.setEmail(createUserRequest.getEmail ());
                user.setFirstName(createUserRequest.getFirstName ());
                user.setLastName(createUserRequest.getLastName ());
                user.setPhoneNumber(createUserRequest.getPhoneNumber ());
                user.setAddress(createUserRequest.getAddress ());
                user.setPassword(createUserRequest.getPassword ());
            User registeredUser = userService.registerUser(user);
            UserDto userDto = modelMapper.map(registeredUser, UserDto.class);
            return new ResponseEntity<>(userDto, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/public/user/{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long userId) {
        try {
            User user = userService.getUserById(userId);
            if (user != null) {

                UserDto userDto = modelMapper.map(user, UserDto.class);
                return new ResponseEntity<>(userDto, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/admin/user/all")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        List<UserDto> userDtos = users.stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect( Collectors.toList());
        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }

    @PutMapping("/public/user/{userId}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long userId, @RequestBody CreateUserRequest createUserRequest) {
        try {

            User exist = userService.getUserById(userId);

            exist.setFirstName(createUserRequest.getFirstName ());
            exist.setLastName(createUserRequest.getLastName ());
            exist.setPhoneNumber(createUserRequest.getPhoneNumber ());
            exist.setEmail(createUserRequest.getEmail ());
            exist.setAddress ( createUserRequest.getAddress () );
            exist.setPassword(createUserRequest.getPassword ());

            User updatedUser = userService.updateUser(exist);

            UserDto updatedUserDto = modelMapper.map(updatedUser, UserDto.class);

            return new ResponseEntity<>(updatedUserDto, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/admin/user/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/admin/user/email")
    public ResponseEntity<UserDto> findUserByEmail(@RequestParam String email) {
        try {
            Optional<User> user = userService.findUserByEmail (email);
            return new ResponseEntity<> ( HttpStatus.NOT_FOUND );
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

package tn.dkSoft.MyTicket.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "venue")
public class Venue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "venueId", unique = true)
    private Long venueId;
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Venue not contain numbers or special characters")
    private String venueName;
    @Pattern(regexp = "^\\d{10}$", message = "Mobile Number must contain only Numbers")
    private long capacity;

    private boolean status;
@JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "venue")
    private List<Session> session;
}

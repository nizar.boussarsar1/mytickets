package tn.dkSoft.MyTicket.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.dkSoft.MyTicket.dto.*;
import tn.dkSoft.MyTicket.model.*;

@Service
public class MapperImpl {

    @Autowired
    private ModelMapper modelMapper;

    public static Session fromSessionDTO(SessionDto sessionDto) {
        Session session = new Session();
        BeanUtils.copyProperties(sessionDto, session);
        return session;
    }

    public static SessionDto fromSession(Session session) {
        SessionDto sessionDto = new SessionDto();
        BeanUtils.copyProperties(session, sessionDto);
        return sessionDto;
    }

    public static Venue fromVenueDTO(VenueDto venueDto) {
        Venue venue = new Venue();
        BeanUtils.copyProperties(venueDto, venue);
        return venue;
    }

    public static VenueDto fromVenue(Venue venue) {
        VenueDto venueDto = new VenueDto();
        try {
            BeanUtils.copyProperties(venue, venueDto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return venueDto;
    }

    public static Event fromEventDto(EventDto eventDto) {
        Event event = new Event();
        BeanUtils.copyProperties(eventDto, event);
        return event;
    }

    public static EventDto fromEvent(Event event) {
        EventDto eventDto = new EventDto();

        BeanUtils.copyProperties(event, eventDto);

        return eventDto;
    }

    public static Tickets fromTicketDto(TicketsDto ticketsDto) {
        Tickets tickets = new Tickets();
        BeanUtils.copyProperties(ticketsDto, tickets);
        return tickets;
    }

    public static TicketsDto fromTicket(Tickets tickets) {
        TicketsDto ticketsDto = new TicketsDto();
        BeanUtils.copyProperties(tickets, ticketsDto);

        return ticketsDto;
    }

    public static Cart fromCartDto(CartDto cartDto) {
        Cart cart = new Cart();
        BeanUtils.copyProperties(cartDto, cart);
        return cart;
    }

    public static CartDto fromCart(Cart cart) {
        CartDto cartDto = new CartDto ();
        BeanUtils.copyProperties(cart, cartDto);
        return cartDto;
    }


        public static CartItem fromCartItemDto(CartItemDto cartItemDto) {
            CartItem cartItem = new CartItem();
            cartItem.setCartItemId(cartItemDto.getCartItemId());
            cartItem.setTickets(fromTicketDto(cartItemDto.getTicketsDto()));
            cartItem.setQuantity(cartItemDto.getQuantity());
            cartItem.setTicketPrice(cartItemDto.getTicketPrice());
            cartItem.setCart(fromCartDto(cartItemDto.getCart ()));
            return cartItem;
    }

    public static CartItemDto fromCartItem(CartItem cartItem) {
        CartItemDto cartItemDto = new CartItemDto ();
        cartItemDto.setCartItemId ( cartItem.getCartItemId () );
        cartItemDto.setTicketsDto ( fromTicket ( cartItem.getTickets () ));
        cartItemDto.setQuantity ( cartItem.getQuantity () );
        cartItemDto.setTicketPrice ( cartItem.getTicketPrice () );
        cartItemDto.setCart ( fromCart ( cartItem.getCart () ) );
        return cartItemDto;
    }

    public static User fromUserDto(UserDto userDto) {
        User user = new User ();
        user.setUserId ( userDto.getUserId () );
        user.setEmail ( userDto.getEmail () );
        user.setPassword ( userDto.getPassword () );
        user.setFirstName ( userDto.getFirstName () );
        user.setLastName ( userDto.getLastName () );
        user.setAddress ( userDto.getAddress () );
        user.setPhoneNumber ( userDto.getPhoneNumber () );
        user.setCart ( fromCartDto ( userDto.getCart () ) );
        return user;
    }
    public static User fromCreateUserRequest(CreateUserRequest createUserRequest) {
        User user = new User();
        BeanUtils.copyProperties(createUserRequest, user);
        return user;
    }


    public static UserDto fromUser(User user) {
        UserDto userDto = new UserDto ();
        userDto.setUserId(user.getUserId());
        userDto.setPassword(user.getPassword());
        userDto.setEmail(user.getEmail());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setAddress(user.getAddress());
        userDto.setCart(fromCart(user.getCart()));
        return userDto;
    }

    public static Order fromOrdertDto(OrderDto orderDto) {
        Order order = new Order();
        BeanUtils.copyProperties(orderDto, order);
        return order;
    }

    public static OrderDto fromOrder(Order order) {
        OrderDto orderDto = new OrderDto ();
        BeanUtils.copyProperties(order, orderDto);
        return orderDto;
    }
  public static OrderItem fromOrderItemDto(OrderItem orderItem) {
        OrderItemDto orderItemDto = new OrderItemDto ();
        BeanUtils.copyProperties(orderItemDto, orderItem);
        return orderItem;
    }

    public static OrderItemDto fromOrderItem(OrderItemDto orderItemDto) {
        OrderItem orderItem = new OrderItem ();
        BeanUtils.copyProperties(orderItem, orderItemDto);
        return orderItemDto;
    }
    public OrderDto mapOrderToOrderDto(Order order) {
        // Create a custom TypeMap to handle the mapping of orderId
        TypeMap<Order, OrderDto> orderTypeMap = modelMapper.createTypeMap(Order.class, OrderDto.class);
        orderTypeMap.addMappings(mapper -> {
            // Specify the mapping of orderId from order to orderDto
            mapper.map(src -> src.getOrderId(), OrderDto::setOrderId);
        });

        return modelMapper.map(order, OrderDto.class);
    }


    public Order fromOrderDto(OrderDto orderDto) {
        return modelMapper.map(orderDto, Order.class);
    }

    public OrderDto fromOrderr(Order order) {
        return modelMapper.map(order, OrderDto.class);
    }




}

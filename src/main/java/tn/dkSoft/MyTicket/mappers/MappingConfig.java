package tn.dkSoft.MyTicket.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tn.dkSoft.MyTicket.dto.OrderDto;
import tn.dkSoft.MyTicket.model.Order;


@Configuration
public class MappingConfig {
    @Bean
    public ModelMapper modelMapper() {


        return new ModelMapper();
    }


}